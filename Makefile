STOG=opam exec -- stog --verbose 2 --package stog.disqus,stog.markdown,stog-math --nocache
STOG_OPTIONS=-d $(DEST_DIR) $(BASE_URL_OPTION) $(PUBLICATION_LEVEL_OPTION) --tmpl tmpl $(STOG_VERBOSE)

LESSC=lessc

DEST_DIR=../gagallium-output
BASE_URL_OPTION=--site-url https://cambium.inria.fr/blog
PUBLICATION_LEVEL_OPTION=

.PHONY: clean build site test local

# [make clean] cleans up.

clean:
	rm -fr $(DEST_DIR)/*
	rm -f ocaml.log

# [make build] builds everything and places the result in ../gagallium-output.

build:
	rm -fr $(DEST_DIR)/*
	$(MAKE) site

# [make test] builds everything and places the result in ../gagallium-test.
# The files are intended to be uploaded to the following test URL:

STOG_TEST_URL ?= http://cambium.inria.fr/~fpottier/gagallium-test

test:
	rm -fR ../gagallium-test/*
	$(MAKE) \
	  BASE_URL_OPTION="--site-url $(STOG_TEST_URL)" \
	  PUBLICATION_LEVEL_OPTION="--publication-level draft" \
	  DEST_DIR="../gagallium-test" \
	  build
	cp ../test-htaccess ../gagallium-test/.htaccess

# [make local] builds everything and places the result in /tmp/gagallium
# where they can be viewed.

local_tmp=/home/angeletti/tmp/gagallium

local:
	$(MAKE) \
	  BASE_URL_OPTION="--site-url file://${local_tmp}" \
	  PUBLICATION_LEVEL_OPTION="--publication-level draft" \
	  DEST_DIR="${local_tmp}" \
	  build

# A private subroutine of the above public commands.

site:
	time $(STOG) $(STOG_OPTIONS) .
	(cd less && $(LESSC) style.less > style.css)
	cp highlight_style/pygments.css $(DEST_DIR)/pygments.css
	cp less/style.css $(DEST_DIR)/
	cp -f *.png tmpl/*png $(DEST_DIR)/ || true
	cp -f *.svg $(DEST_DIR)/ || true
