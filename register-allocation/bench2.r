library(ggplot2)
# header = TRUE ignores the first line, check.names = FALSE allows '+' in 'C++'

benchmark <- read.table("bench2.dat", 
	header = TRUE, row.names = "Title")

# 't()' is matrix tranposition, 'beside = TRUE' separates the benchmarks,
# 'heat' provides nice colors

png("bench2.png", height=300, width=500)
par(las=2)
par(oma=c(2,0,0,0))
barplot(t(as.matrix(benchmark)), beside = TRUE, col = heat.colors(2))

# 'cex' stands for 'character expansion', 'bty' for 'box type' (we don't want
# borders)

legend("topleft", names(benchmark), cex = 0.5, bty = "n", fill = heat.colors(2))
