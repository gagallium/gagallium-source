<!-- Generator: GNU source-highlight 3.1.6
by Lorenzo Bettini
http://www.lorenzobettini.it
http://www.gnu.org/software/src-highlite -->
<pre><tt>article-title=Technical Details: Static Blogging in OCaml with Stog
article-date=2012/05/08
article-published= true
author=Gabriel Scherer
topics=blog
keywords=stog, xml, latex, markdown, mathjax
languages=
&lt;-&gt;
<b><font color="#0000FF">&lt;p&gt;</font></b>Gagallium uses the static blog
generator <b><font color="#0000FF">&lt;a</font></b> <font color="#009900">href</font><font color="#990000">=</font><font color="#FF0000">"http://zoggy.github.com/stog/"</font><b><font color="#0000FF">&gt;</font></b>Stog<b><font color="#0000FF">&lt;/a&gt;</font></b>, recently
released by Maxence Guesdon. The nice thing with using a static blog
generator is that deployment is simplified a lot.<b><font color="#0000FF">&lt;/p&gt;</font></b>
<b><font color="#0000FF">&lt;sep_/&gt;</font></b>
<i><font color="#9A1900">&lt;!-- we explicitely use &lt;![CDATA[..]]&gt; below to make sure that Xtmpl</font></i>
<i><font color="#9A1900">     does not try to preprocess the markdown below before passing it</font></i>
<i><font color="#9A1900">     to `markdown` --&gt;</font></i>
<b><font color="#0000FF">&lt;markdown&gt;</font></b>&lt;![CDATA[
### Why a static blog generator

The biggest pain in a blog administration is handling the
comments. Hunting spam, learning about modern identification
protocols, all of this is time-consuming and frankly boring. I believe
there are two reasonable choices:

- host one's entire blog on a central service that provides spam
filtering and moderation tools
- host the blog locally but offsource the comment handling to
a dedicated commenting service that provides spam filtering and
moderation tools

The problem with the first option is that it can be limiting in what
can be used in the blog. We considered using a centrally hosted
[Wordpress](http://wordpress.com) blog for example, which provide
a reasonable set of features (including source code highlighting), but
what if we want, for example, automatic type-checking of the OCaml
snippets we put on the blog?

I therefore decided to adopt the second option: we use the commenting
service [Disqus](http://disqus.com/) that provides comment
administration tools. My main gripe with Disqus is that it's not free
software; there are plans for free alternatives, but nothing ready
yet, at least not to my knowledge -- I'd be delighted to find one.

Comments really are the only component of a blog that needs to be
dynamic. By using javascript to contact the commenting service, we let
users handle it, and a static blog is therefore a very compelling
option. Dead simple deployment and administration, few security
worries, etc.

### About stog

[Stog](http://zoggy.github.com/stog/) is a very young blog generator;
as I understand, it evolved from Maxence Guesdon successive hardcoded
websites, and was released just a few weeks ago. You have to be ready
to hack, and to tolerate a certain level of idiosyncrasy, to use it,
but I am overall quite happy; and the hackability is also a feature.

It seemed fun to run a blog on OCaml, though I was initially afraid of
intoxication with the Not Invented Here syndrome. I'm sure there are
good (maybe better) static blog generators written in other languages,
but I think it has potential to become a very reasonable alternative.

Stog makes it easy to highlight OCaml code, have a toplevel run it to
check for obvious mistakes and potentially print back results.
<b><font color="#0000FF">&lt;ocaml-eval&gt;</font></b>
let x = 1;;
<b><font color="#0000FF">&lt;/ocaml-eval&gt;</font></b>
<b><font color="#0000FF">&lt;ocaml-eval</font></b> <font color="#009900">toplevel</font><font color="#990000">=</font><font color="#FF0000">"true"</font><b><font color="#0000FF">&gt;</font></b>
let y = x;;
<b><font color="#0000FF">&lt;/ocaml-eval&gt;</font></b>

There is also a preprocessor for LaTeX code, allowing to easily use
math formulas. I originally planned to use the nice
[MathJax](http://www.mathjax.org/) javascript layer (client-side
rendering of LaTeX), but most syndication feed readers don't accept
javascript.. for now -- I hope they start doing it, because otherwise
they could be obsoleted, and I like the efficient workflow of RSS/Atom
readers.

<b><font color="#0000FF">&lt;latex&gt;</font></b>
 \forall \sigma \forall \tau,\qquad
  \sigma \leq \tau
  \quad \Longrightarrow \quad 
  \sigma\;\mathtt{t} \leq \tau\;\mathtt{t} 
<b><font color="#0000FF">&lt;/latex&gt;</font></b>

Stog's design is also quite simple and very reasonable. In the end, it
is a metadata-gathering layer (to generate archives, automatically
compute previous/next links, etc.) on top of a simple and flexible
templating engine, [Xtmpl](https://github.com/zoggy/xtmpl). The code
is not pretty (that can be cured), but works well. It is a rewrite
engine that transforms an input XML document until it reaches
a fixpoint (using Daniel Bünzli's
[Xmlm](http://erratique.ch/software/xmlm) library). The transformation
is completely parametrized by the user, which can provide translation
functions for any element (say `<b><font color="#0000FF">&lt;markdown&gt;</font></b>`).

Stog comes with some predefined active elements
(`<b><font color="#0000FF">&lt;include</font></b> <font color="#009900">file</font><font color="#990000">=</font><font color="#FF0000">"foo"</font><b><font color="#0000FF">/&gt;</font></b>`, `<b><font color="#0000FF">&lt;if</font></b> <font color="#009900">var</font><font color="#990000">=</font><font color="#FF0000">"val"</font><b><font color="#0000FF">&gt;</font></b>`), and it is easy to
add new ones. The discus integration is implemented by providing
a `<b><font color="#0000FF">&lt;comments/&gt;</font></b>` command, and I have added
a [markdown](http://daringfireball.net/projects/markdown/) processing
plugin.

Finally, I have found Maxence to be a nice maintainer to work with:
I proposed a few changes, and they have been integrated upstream.

### An example

You can look at the source for this post:
[index.src.html](<b><font color="#000080">&amp;lt;</font></b>article-url/<b><font color="#000080">&amp;gt;</font></b>/index.src.html)

What is not shown here is the set of templates I wrote to imitate the
existing design of the [Gallium](http://gallium.inria.fr/)
website. They were adapted from the templates distributed with Stog,
which are to the one used to build its website (the complete source
for the website can be found in the repository, in
[doc/](https://github.com/zoggy/stog/tree/master/doc)). Writing those
design templates is an initial investment that takes a bit of time,
but hopefully the delightful red stripes will be a big asset for
future OCaml propaganda.
]]&gt;<b><font color="#0000FF">&lt;/markdown&gt;</font></b>
</tt></pre>
